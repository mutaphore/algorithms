#!/usr/local/bin/python


def pprint_header(V):
    "Pretty print the header"
    print "{0:^8}".format("N"),
    for v in V:        
        print "{0:^15}".format(v),
    print


def pprint_row(V, D, p, N):
    "Pretty print the row"
    print "{0:^8}".format("".join(N)),
    for v in V:
        if not D.get(v) and not p.get(v):
            print "{0:^15}".format(""),
        elif D[v] == float("inf"):
            print u"{0:^15}".format(u'\u221e'),
        else:
            print "{0:>7},{1:7}".format(D[v], p[v]),
    print


def dijkstra(adj_list, source):
    "Run Dijkstra's algorithm and print the steps"
    # Initialization
    V = sorted(adj_list.keys())
    V.remove(source)  # All vertices except source
    D = {}  # Distance from source to vertex
    p = {}  # Parent vertex
    N = [source]
    for v in V:
        if v in adj_list[source].keys():
            D[v] = adj_list[source][v]
            p[v] = source
        else:
            D[v] = float("inf")
            p[v] = None
    pprint_header(V)
    pprint_row(V, D, p, N)
    # Run the algorithm
    while set(N) != set(adj_list.keys()):
        # Find minimum distance
        min_d = float("inf")
        min_v = None
        for vert, dist in D.items():
            if vert not in N and dist < min_d:
                min_d = dist
                min_v = vert
        N.append(min_v)
        D.pop(min_v)
        p.pop(min_v)
        # Update neighbors of min_v
        for vert, dist in adj_list[min_v].items():
            if vert not in set(N) and min_d + dist < D[vert]:
                D[vert] = min_d + dist
                p[vert] = min_v
        pprint_row(V, D, p, N)



if __name__ == "__main__":
    adj_list = {
        "t": {"u": 2, "v": 4, "y": 7},
        "u": {"t": 2, "v": 3, "w": 3},
        "v": {"t": 4, "u": 3, "w": 4, "x": 3, "y": 8},
        "w": {"u": 3, "v": 4, "x": 6},
        "x": {"z": 8, "y": 6, "v": 3, "w": 6},
        "y": {"t": 7, "v": 8, "x": 6, "z": 12},
        "z": {"y": 12, "x": 8}
    } 

    dijkstra(adj_list, "t")
    dijkstra(adj_list, "u")
    dijkstra(adj_list, "v")
    dijkstra(adj_list, "w")
    dijkstra(adj_list, "y")
    dijkstra(adj_list, "z")