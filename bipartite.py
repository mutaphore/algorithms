#!/usr/local/bin/python

"""
Given some graph represented by an adjacency 
list, determine if it is a bipartite.
"""

class Vertex(object):
    def __init__(self, v):
        self.name = v  # Vertex identifier
        self.color = "WHITE"  # Color can be "WHITE", "GRAY" or "BLACK"
        self.t = None  # Type = 1 for "GOOD", 0 for "BAD". None if not visited

    def __hash__(self):
        return hash(self.name)


def is_bipartite(L, adj_list):
    "Given a list of vertices and adjacency list, is it a bipartite?"
    for v in L:
        if v.color == "WHITE" and BFS(v, adj_list) == False:
            return False 
    return True

def BFS(s, adj_list):
    "Do BFS from source vertex s"
    queue = []
    s.color = "GRAY"
    s.t = 1
    queue.append(s)
    while len(queue) > 0:
        v = queue.pop(0)
        for adj in adj_list[v]:
            if adj.color == "WHITE":
                adj.color = "GRAY"
                adj.t = v.t ^ 1  # Flip to a different type
                queue.append(adj)
            elif adj.color == "BLACK" and adj.t == v.t:
                return False
        v.color = "BLACK"
    return True 


if __name__ == "__main__":
    R = Vertex("R")
    S = Vertex("S")
    T = Vertex("T")
    U = Vertex("U")
    V = Vertex("V")
    W = Vertex("W")
    X = Vertex("X")
    Y = Vertex("Y")
    L = [R, S, T, U, V, W, X, Y]

    adj_list = {}
    adj_list[R] = [S, V]
    adj_list[S] = [R, W]
    adj_list[T] = [U, W, X]
    adj_list[U] = [T, X, Y]
    adj_list[V] = [R]
    adj_list[W] = [S, T, X]
    adj_list[X] = [T, U, W, Y]
    adj_list[Y] = [U, X]

    #A = Vertex("A")
    #B = Vertex("B")
    #C = Vertex("C")
    #D = Vertex("D")
    #L = [A, B, C, D]
    #adj_list[A] = [B, C]
    #adj_list[B] = [A, D]
    #adj_list[C] = [A, D]
    #adj_list[D] = [B, C]
    print "Bipartite = %r" % is_bipartite(L, adj_list)
