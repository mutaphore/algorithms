#!/usr/local/bin/python

"""
Given number of nodes N, price of node u: p[u] 1<=u<=N, find
the cheapest Binary tree structure of the nodes. The cost of
a node is p[u]*d[u], where d[u] represents the depth of the
node from root of the tree.
"""

import os, sys

sys.path.append(os.getenv("HOME") + "/Code/Python/libst")
from bst import BST

def min_cost(N, p):
    "Create the min cost matrix and list of tree roots k"
    # Create cumulative p for faster processing
    pcum = [0] * (N + 1)
    for i in range(1, N + 1):
        pcum[i] = pcum[i-1] + p[i]
    # Cost matrix
    c = [[0] * (N + 2) for i in range(N + 2)]
    # Root node of the optimal tree from i to j
    mid = [[0] * (N + 1) for i in range(N + 1)]
    for i in range(1, N + 1):
        mid[i][i] = i
    # Calculate the values
    for d in range(2, N + 1):
        for i in range(1, N - d + 2):
            j = i + d - 1
            min_c = float("inf")
            min_k = i
            for k in range(i, j + 1):
                temp = c[i][k-1] + c[k+1][j] + pcum[j] - pcum[i-1] - p[k]
                if temp < min_c:
                    min_c = temp
                    min_k = k
            c[i][j] = min_c
            mid[i][j] = min_k
    return c, mid


def tree_build(i, j, w, mid, nodes):
    "Builds the optimal tree recursively"
    if i == j:
        if i < w:
            nodes[w].left = nodes[i]
        else:
            nodes[w].right = nodes[i]
        return
    k = mid[i][j] 
    # Handle starting case
    if w == None:
        w = k
    if i < k < j:
        if k < w:
            nodes[w].left = nodes[k]
        else:
            nodes[w].right = nodes[k]
        tree_build(i, k-1, k, mid, nodes)
        tree_build(k+1, j, k, mid, nodes)
    elif i == k:
        if k < w:
            nodes[w].left = nodes[k]
        else:
            nodes[w].right = nodes[k]
        tree_build(i+1, j, i, mid, nodes)
    elif j == k:
        if k < w:
            nodes[w].left = nodes[k]
        else:
            nodes[w].right = nodes[k]
        tree_build(i, j-1, j, mid, nodes)


if __name__ == "__main__":
    # Generate a list of nodes (index start at 1)
    N = 5
    p = [None, 0.6, 0.15, 0.1, 0.01, 0.14]
    c, mid = min_cost(N, p)

    for row in c:
        print row
    print
    for row in mid:
        print row

    nodes = [None] + [BST.Node(i) for i in range(1, N+1)]
    tree_build(1, N, None, mid, nodes)

    bst = BST() 
    bst.root = nodes[mid[1][N]]
    print nodes[1].left
    print nodes[1].right
    print nodes[2].left
    print nodes[2].right
    print nodes[3].left
    print nodes[3].right
    print nodes[4].left
    print nodes[4].right
    print nodes[5].left
    print nodes[5].right
    bst.print_tree("INORDER")
