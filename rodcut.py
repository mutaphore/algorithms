#!/usr/local/bin/python

def rodcut(n, P):
	"Given p list of prices for cuts, maximize the revenue of rod length n"	
	revs = [0] * (n + 1)
	cuts = []
	for i in range(1, n + 1):
		largest = P[i]
		cut = i
		for j in range(1, i):
			rev = revs[j] + revs[i-j]
			if rev > largest:
				largest = rev
				cut = j
		revs[i] = largest
		cuts.append(cut)
	print revs[n]
	print cuts


def rodcut_class(n, P):
	"Given p list of prices for cuts, maximize the revenue of rod length n"	
	revs = [0] * (n + 1)
	cuts = [0] * (n + 1)
	for i in range(1, n + 1):
		largest = 0
		for j in range(1, i + 1):
			rev = P[j] + revs[i-j]
			if rev > largest:
				largest = rev
				cuts[i]	= j
		revs[i] = largest
	print revs[n]
	# Print the cuts
	rem = n
	while rem > 0:
		print cuts[rem]
		rem -= cuts[rem]


if __name__ == "__main__":
    n = 10
    P = [None, 1, 7, 4, 5, 8, 8, 7, 9, 9 ,10]
    rodcut(n, P)
    rodcut_class(n, P)
